#### Un poco de historia
- Al igual que los comunes tradiconales, el sw libre (aunque no se llamaba así) era la manera de funcionar entre los hackers de departamentos universitarios (y otras instituciones), compartir el código es tan antiguo como los ordenadores. (comunidades)
- hacking: explorar  los  límites  de  lo  posible  con  un  espíritu  de  sagacidad  imaginativa
- Con el cambio de generación, al introducirse los VAX o el 68020, incluían sistemas operativos privativos. (por qué privativos y no propietarios?)
- antes  de  poder  utilizar  un  ordenador tenías  que  prometer  no  ayudar  a  tu  vecino.  Quedaban así  prohibidas las comunidades cooperativas.
- Las comunidades hacker (con Stallman a la cabeza, no por ser el más listo sino el más loco) tienen que elegir entre 3 estrategias:
  - Aceptar las nuevas condiciones, firmar acuerdos de confidencialidad y prometer no ayudar a sus compañeros hackers.
  - Abandonar el mundo de los ordenadores
  - Estudiar la manera en que un programador podría hacer algo por el bien común -> primer requisito: crear un sistema operativo libre (sin él, ni siquiera puedes hacer funcionar un ordenador).
- SO compatible con UNIX para facilitar su uso y la migración de sistemas existentes.
- El nombre de GNU fue elegido según una tradición de los hackers, como un acrónimo recursivo de «GNU’s Not Unix».
- ¿Qué hace falta para escribir un SO libre?
  - Un editor de texto (Emacs 1984)
  - Financiación (FSF 1985) (Stallman había dejado su trabajo en le MIT para dedicarse fulltime a GNU)
  - Un compilador (GCC 1987)
  - Una licencia pública libre (GPL 1989)
  - Miles de componentes (  libc, bash, tar, gzip, make...)
  - *Y un kernel*
- En 1990 GNU está casi completo, comienza el desarrollo de _Hurd_
- En 1991, Linus Torvalds comienza el desarrollo de un kernel compatible con Unix.
- 1992 GNU + Linux = GNU/Linux
- 2017 Más de 92 millones de usuarios de GNU/Linux en el mundo. (https://www.linuxcounter.net/statistics)

#### Las 4 libertades
- Free as in "free speech" not as in "free beer"
- No tiene ninguna relación con el precio. Lo que nos interesa es la libertad.
- ¿Cuando es libre un software? siempre que, como usuario particular, tengas:
  - 0 La libertad de ejecutar el programa sea cual sea el propósito.
  - 1 La libertad de modificar el programa para ajustarlo a tus necesidades. (implica el libre acceso al código fuente)
  - 2 La libertad de redistribuir copias, ya sea de forma gratuita, ya sea a cambio del pago de un precio.
  - 3 La libertad de distribuir versiones modificadas del programa, de tal forma que la comunidad pueda aprovechar las mejoras introducidas.
- Free software vs Open source:
  - Los términos «software libre» y «código abierto» describen más o menos la misma categoría de software, pero implican cosas muy dis-
tintas  acerca  del  software  y  sus  valores.
  - Los partidarios de este término trataban de evitar  la  confusión entre  «libre»  y  «gratuito»
  - Un objetivo muy legítimo.
  - Otros intentaban dejar a un lado los principios del SL para atraer a los ejecutivos y usuarios empresariales.
  - la retórica del «código abierto» se concentra en la posibilidad de crear un software de alta calidad y capacidad, pero rehuye las nociones de libertad, comunidad y principios.

https://www.fsf.org/blogs/rms/20140407-geneva-tedx-talk-free-software-free-society (hasta 7:30)
