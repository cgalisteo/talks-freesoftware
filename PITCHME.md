@title[Introducción al software libre]

### Introducción al Software Libre

<a href="http://www.fsfe.org/freesoftware/freesoftware.html"><img src="http://www.fsfe.org/contribute/advocacy/cwfs/cwfs-1.0.0-original-25-degree-300x201.png" width="300" height="201" border="0" alt="Created with free software"></a>


[Libro: Software libre para una sociedad libre](http://www.gnu.org/philosophy/fsfs/free_software.es.pdf)



---

## Un poco de Historia

- Compartir el código es tan antiguo como los ordenadores.
- El sw libre era la manera de funcionar entre los *hackers* de los departamentos universitarios y otras instituciones (aunque no se llamara así aun)

Note:
- Parecido a lo que pasa con los comunes tradicionales: lo normal se convierte en extraordinario.
- El concepto de hacker relativo a la seguridad cabe en la definición, pero es confundir la parte por el todo.

+++
### Un inciso sobre la ética hacker

- Hacking: «explorar los límites de lo posible con un espíritu de sagacidad  imaginativa.»

- Pekka Himanen «La ética del hacker y el espíritu de la era de la información»
  - Etica hacker como una nueva relación con el tiempo, el dinero y el trabajo opuestas a la ética protestante en la que Max Weber vió los orígenes del capitalismo.

Note:


### Un inciso sobre la ética hacker
+++
- Erick S. Raymond «How To Become A Hacker»:
  - El mundo está lleno de problemas fascinantes que esperan ser resueltos (requiere mucho esfuerzo y el esfuerzo requiere motivación)
  - Ningún problema tendría que resolverse dos veces (el tiempo para pensar que emplean otros hackers tan precioso que es casi una obligación moral compartir la información)
  - El aburrimiento y el trabajo rutinario son perniciosos (automatizar las tareas rutinarias)
  - La libertad es buena (antiautoritarios)
  - La actitud no es sustituto para la competencia (desconfiar de la actitud y respetar la competencia, do-ocracy)

Note:
[LIBERTAD] Cualquiera que pueda darte órdenes, puede obligarte a dejar de resolver ese problema que te está fascinando —y, dada la manera como trabajan las mentes autoritarias, encontrarán alguna razón espantosamente estúpida para hacerlo

[ACTITUD] Igual que el otro día hablablamos, al hilo de Bourdieu de cómo aunque tengas un rasgo no está bien visto alardear de él, hay una tradición hacker de no llamarse hacker a uno mismo, es algo que te llaman los demás.

---
Con el cambio de generación, al introducirse los VAX y ordenadores basados en el micropocesador 68020, los fabricantes comienzan a incluir sistemas operativos privativos.

---
### Pero volvamos a nuestra historia

Ojo, que hablamos de estos tiempos...

![VAX-8350](https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/DEC-VAX-8350-front-0a.jpg/440px-DEC-VAX-8350-front-0a.jpg)

---

@fa[frown-o fa-4x]

- Antes  de  poder  utilizar  un  ordenador tenías  que  prometer  no  ayudar  a  tu  vecino.  
- Quedaban así  prohibidas las comunidades cooperativas. |

---
#### Las comunidades hacker tienen que elegir entre 3 estrategias:
  - Aceptar las nuevas condiciones, firmar acuerdos de confidencialidad y prometer no ayudar a sus compañeros hackers. |
  - Abandonar el mundo de los ordenadores |
  - Estudiar la manera en que un programador podría hacer algo por el bien común |

---
#### ¿Cual creeis que eligieron?

- Efectivamente, ponerse manos a la obra con Stallman a la cabeza (no por ser el más listo sino el más loco) |
- ![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Richard_Stallman_-_Preliminares_2013.jpg/220px-Richard_Stallman_-_Preliminares_2013.jpg) |

---
#### primer requisito: crear un sistema operativo libre (sin él, ni siquiera puedes hacer funcionar un ordenador).
  ![OS](http://i1.wp.com/idiotechie.com/wp-content/uploads/2012/03/Unix-Architecture-Layers.jpg?resize=823%2C450)
---
## GNU
![GNU](https://upload.wikimedia.org/wikipedia/en/thumb/2/22/Heckert_GNU_white.svg/246px-Heckert_GNU_white.svg.png)

- SO compatible con UNIX para facilitar su uso y la migración de sistemas existentes. |
- El nombre de GNU fue elegido (siguiendo una tradición hacker) como un acrónimo recursivo de «GNU’s Not Unix». |
---

### ¿Qué hace falta para escribir un S.O libre? (aparte de estar un poco pirado)
- Un editor de texto* (Emacs 1984) |
- Financiación (FSF 1985) (Stallman había dejado su trabajo en el MIT para dedicarse fulltime a GNU) |
- Un compilador (GCC 1987) |
- Una licencia pública libre (GPL 1989) |
- Miles de componentes (  libc, bash, tar, gzip, make...) (1990+)|

---

## ... Y un kernel

- En 1990 GNU está casi completo, comienza el desarrollo de Hurd |
- En 1991, Linus Torvalds comienza el desarrollo de un kernel compatible con Unix. |
- 1992: GNU + Linux = GNU/Linux |
- ... |
- 2017: Más de 92 millones de usuarios de GNU/Linux en el mundo. [linuxcounter.net] |

---?image=https://hackadaycom.files.wordpress.com/2016/07/editor-wars-featured.jpg&size=auto 70%

### Editor wars
- https://en.wikipedia.org/wiki/Editor_war#
---
## ¿De qué hablamos cuando hablamos de "Software libre"?
- Free as in "free speech" not as in "free beer" |
- No tiene ninguna relación con el precio. Lo que nos interesa es la libertad. |

---

### ¿Cuando es libre un software?

Siempre que, como usuario particular, tengas:
- 0: La libertad de ejecutar el programa sea cual sea el propósito. |
- 1: La libertad de modificar el programa para ajustarlo a tus necesidades. (implica el libre acceso al código fuente) |
- 2: La libertad de redistribuir copias, ya sea de forma gratuita, ya sea a cambio del pago de un precio. |
- 3: La libertad de distribuir versiones modificadas del programa, de tal forma que la comunidad pueda aprovechar las mejoras introducidas. |


---
### "Free Software" vs "Open source"
- Los términos «software libre» y «código abierto» describen más o menos la misma categoría de software, pero implican cosas muy distintas:
  - Los partidarios de este término trataban de evitar la confusión entre «libre» y «gratuito» (un objetivo muy legítimo) |
  - Pero otros intentaban dejar a un lado los principios del SL para atraer a los ejecutivos y usuarios empresariales. |
  - la retórica del «código abierto» se concentra en la posibilidad de crear un software de alta calidad y capacidad,pero rehuye las nociones de libertad, comunidad y principios. |

---?video=http://188.226.140.235/Stallman.TEDx.Geneva.mp4
---
### ¿Privativo o propietario?

![](https://memegenerator.net/img/images/600x600/984/philosoraptor.jpg)

+++

### ¿Entonces Microsoft podría hacer software libre?
![](https://media.giphy.com/media/gngO1gmBhS9na/giphy.gif)
https://opensource.microsoft.com/ |
https://open.microsoft.com/|

+++
### Algunos conceptos clave

- Source code |
- Repositorio |
- Github |
- Committer |
- Fork |
- Bug |
- Pul Request (PR) |
- Distribución (GNU/Linux) |
